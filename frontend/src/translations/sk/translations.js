export const TRANSLATIONS_SK = {
    header: {
        projectsNav: 'Projekty',
        loginNav: 'Prihlásenie',
        logoutNav: 'Odhlásiť sa',
    },
    projects: {
        projectNameSk: 'Názov projektu (slovensky)',
        projectNameEn: 'Názov projektu (anglicky)',
        projectImage: 'Obrázok projektu',
        projectUrl: 'URL projektu',
        paragraph: 'Popis projektu',
        languageSk: 'slovensky',
        languageEn: 'anglicky',
        projectTechnology: 'Technológia',
        projectTechnologyUrl: 'URL na technológiu',
        submit: 'Vložiť',
    },
    login: {
        userName: 'Používateľ',
        password: 'Heslo',
        submit: 'Vstúpiť',
    },
    projectCard: {
        delete: 'Vymazať',
    }
};