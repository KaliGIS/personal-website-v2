export const TRANSLATIONS_EN = {
    header: {
        projectsNav: 'Projects',
        loginNav: 'Login',
        logoutNav: 'Logout',
    },
    projects: {
        projectNameSk: 'Project name (slovak)',
        projectNameEn: 'Project name (english)',
        projectImage: 'Project image',
        projectUrl: 'Project URL',
        paragraph: 'Project paragraph',
        languageSk: 'slovak',
        languageEn: 'english',
        projectTechnology: 'Technology',
        projectTechnologyUrl: 'Technology URL',
        submit: 'Submit',
    },
    login: {
        userName: 'User',
        password: 'Password',
        submit: 'Submit',
    },
    projectCard: {
        delete: 'Vymazať',
    }
};