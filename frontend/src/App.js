import './App.css';
import { Switch, Route, useHistory } from 'react-router-dom';
import Header from './components/Header';
import Projects from './components/Projects';
import Login from './components/Login';
import Cookies from 'js-cookie';
import { Fragment, useState, createContext } from 'react';
import i18n from './translations/i18n';

export const AppContext = createContext({loggedUser: null, currentLanguage: i18n.language});

function App() {
  const [loggedUser, setLoggedUser] = useState(null);
  const [currentLanguage, setCurrentLanguage] = useState(i18n.language);
  const history = useHistory();

  const logoutCallback = () => {
    console.log(`Setting user to null`);
    setLoggedUser(null);

    const requestOptions = {
      method: 'DELETE',
      headers: {
          'Content-Type': 'application/json',
          'X-Csrftoken': Cookies.get('csrftoken'),
      },
  }

    fetch('/api/login', requestOptions);
    history.push('/login');
  }

  return (
    <AppContext.Provider value={{loggedUser, setLoggedUser, logoutCallback, currentLanguage, setCurrentLanguage}}>
        <Header />
        <Switch>
          <Route path="/projects">
              <Projects />
          </Route>
          <Route path="/login">
              <Login />
          </Route>
        </Switch>
    </AppContext.Provider>
  );
}

export default App;
