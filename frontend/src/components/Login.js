import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router';
import { TextField, Grid, Button, Snackbar, Alert } from '@mui/material';
import { styled } from '@mui/material/styles';
import '../translations/i18n';
import { useTranslation } from 'react-i18next';
import Cookies from 'js-cookie';
import { AppContext } from '../App';

const StyledGridContainer = styled(Grid)(({theme}) => ({
    'marginTop': '8px'
}));

const StyledGridItem = styled(Grid)(({theme}) => ({
    textAlign: 'center'
}));

function Login() {
    const history = useHistory();
    const { t } = useTranslation();
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [isSnackbarOpened, setIsSnackbarOpened] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');
    const { setLoggedUser } = useContext(AppContext);

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Button clicked', userName, password);

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-Csrftoken': Cookies.get('csrftoken'),
            },
            body: JSON.stringify({
                username: userName,
                password: password
            })
        }

        fetch('/api/login', requestOptions)
        .then(response => {
            console.log(response)
            if (response.status === 200) {
                return response.json();
            }
            return response.json().then(errorJson => {throw new Error(errorJson.message)});
        })
        .then(responseJson => {
            console.log(responseJson);
            if (responseJson['user'] !== null) {
                setLoggedUser(responseJson['user']);
            }
            history.push('/projects');
        })
        .catch(error => {
            setErrorMsg(error.message);
            setIsSnackbarOpened(true);
        })
    }

    const handleClose = (e) => {
        setErrorMsg('');
        setIsSnackbarOpened(false);
    }

    return (
        <form
            onSubmit={handleSubmit}
        >
            <StyledGridContainer container spacing={1}>
                <StyledGridItem item xs={12}>
                    <TextField
                        name="userName"
                        label={t('login.userName')}
                        variant="outlined"
                        onChange={(e) => setUserName(e.target.value)}
                    >
                        UserName
                    </TextField>
                </StyledGridItem>
                <StyledGridItem item xs={12}>
                    <TextField
                        name="password"
                        label={t('login.password')}
                        variant="outlined"
                        type="password"
                        onChange={(e) => setPassword(e.target.value)}
                    >
                        Password
                    </TextField>
                </StyledGridItem>
                <StyledGridItem item xs={12}>
                    <Button
                        variant="contained"
                        color="warning"
                        type="submit"
                    >
                        {t('login.submit')}
                    </Button>
                </StyledGridItem>
                <Snackbar 
                    open={isSnackbarOpened} 
                    autoHideDuration={5000} 
                    onClose={handleClose} 
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                >
                    <Alert severity="error" onClose={handleClose}>
                        {errorMsg}
                    </Alert>
                </Snackbar>
            </StyledGridContainer>
        </form>
    )
}

export default Login;
