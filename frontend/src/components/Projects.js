import React, { useEffect, useState, useContext } from 'react';
import { styled } from '@mui/material/styles';
import { Container, Fab } from '@mui/material';
import Masonry from '@mui/lab/Masonry';
import MasonryItem  from '@mui/lab/MasonryItem';
import AddIcon from '@mui/icons-material/Add';
import ProjectCard from './ProjectCard';
import NewProject from './NewProject';
import Cookies from 'js-cookie';
import { AppContext } from '../App';

const PREFIX = 'Projects';

const classes = {
    gridItem: `${PREFIX}-gridItem`,
    fab: `${PREFIX}-fab`
};

const StyledContainer = styled(Container)((
    {
        theme
    }
) => ({
    paddingTop: '8px',
    [`& .${classes.gridItem}`]: {
        padding: '4px',
    },

    [`& .${classes.fab}`]: {
        position: 'fixed',
        top: 'auto',
        right: '20px',
        bottom: '20px',
        left: 'auto',
    }
}));

function Projects() {
    const [projects, setProjects] = useState([]);
    const [openNewProject, setOpenNewProject] = useState(false);
    const { loggedUser, currentLanguage } = useContext(AppContext);
    console.log(`Current language: ${currentLanguage}`);

    const getProjects = () => {
        fetch('/api/projects', {
            headers: {
                'Accept-Language': currentLanguage,
            }
        })
        .then(response => {
            if (response.status === 200) {
                return response.json();   
            }
            return [];
        }).then(data => {
            console.log(data);
            setProjects(data);
        });
    }

    useEffect(() => {
        getProjects();
    }, [currentLanguage]);

    const handleFabClick = (event) => {
        event.preventDefault();
        console.log('Fab clicked');
        setOpenNewProject(true);
    }

    const handleModalClose = () => {
        setOpenNewProject(false);
    }

    const renderFab = () => {
        return (
            <Fab 
                className={classes.fab} 
                color='accent'
                onClick={handleFabClick}
            >
                <AddIcon />
            </Fab>
        );
    }

    const handleProjectDelete = (id) => {
        fetch(`api/projects/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'X-Csrftoken': Cookies.get('csrftoken'),
            },
        })
        .then(res => {
            if (res.status === 204) {
                let updatedProjects = projects.filter(project => {
                    if (project.id !== id) {
                        return project;
                    }
                });
                setProjects(updatedProjects);
            }
        });
    }
    
    const setNewProjectCallback = (newProject) => {
        let updatedProjects = [...projects];
        updatedProjects.push(newProject);
        setProjects(updatedProjects);
    }

    return (
        <StyledContainer maxWidth='lg'>
            <Masonry columns={{xs: 1, sm: 2, md: 3, lg: 4}} spacing={1}>
                {projects.map((project) => (
                    <MasonryItem key={project.id}>    
                        <ProjectCard 
                            title={project.title}
                            imageLink={project.imageLink}
                            paragraphs={project.projectparagraph_set}
                            technologies={project.projecttechnology_set}
                            link={project.link}
                            loggedUser={loggedUser}
                            id={project.id}
                            projectDeleteCallback={handleProjectDelete}
                        />
                    </MasonryItem>
                ))}
            </Masonry>
            { loggedUser && renderFab() }
            <NewProject opened={openNewProject} onClose={handleModalClose} setNewProjectCallback={setNewProjectCallback} />
        </StyledContainer>
    );
}

export default Projects;
