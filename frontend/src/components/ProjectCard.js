import { styled } from '@mui/material/styles';
import {
    Card,
    CardHeader,
    CardContent,
    CardMedia,
    Typography,
    Chip,
    Link,
    CardActionArea,
    IconButton,
    CardActions,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import { useTranslation } from 'react-i18next';
import '../translations/i18n';

const PREFIX = 'ProjectCard';

const classes = {
    link: `${PREFIX}-link`,
    projImage: `${PREFIX}-projImage`
};

const StyledCard = styled(Card)((
    {
        theme
    }
) => ({
    [`& .${classes.link}`]: {
        margin: theme.spacing(0.5),
    },

    [`& .${classes.projImage}`]: {
        maxHeight: '400px',
        objectFit: 'contain',
    }
}));

function ProjectCard({title, link, imageLink, paragraphs, technologies, loggedUser, id, projectDeleteCallback}) {
    const { t } = useTranslation();

    const renderDeleteButton = (id) => {
        return(
            <IconButton onClick={() => projectDeleteCallback(id)}>
                <DeleteIcon />
            </IconButton>
        );
    }
    
    return (
        <StyledCard>
            <CardActionArea>
                <Link 
                    href={link}
                    target="_blank"
                    rel="noopener"
                >
                    <CardHeader 
                        title={title}
                    />
                    <CardMedia 
                        className={classes.projImage}
                        component="img"
                        image={imageLink}
                        title={title}
                    />
                </Link>
            </CardActionArea>
            <CardContent>
                {paragraphs.map((p, index) => {
                    return <Typography key={index} paragraph color="textSecondary">{p.text}</Typography>
                })}
                {technologies.map((tech, index) => {
                    return (
                        <Chip 
                            key={index} 
                            className={classes.link}
                            label={tech.technology}
                            color="secondary"
                            component="a"
                            href={tech.link}  
                            target="_blank"
                            rel="noopener"
                            clickable
                        />
                    )
                })}
            </CardContent>
            <CardActions>
                {
                    loggedUser && renderDeleteButton(id)
                }
            </CardActions>
        </StyledCard>
    );
}

export default ProjectCard;
