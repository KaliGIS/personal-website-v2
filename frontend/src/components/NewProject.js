import React, { useState, useReducer } from 'react';
import { Modal, TextField, Container, Button, IconButton, Grid, Typography } from '@mui/material';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import { useTranslation } from 'react-i18next';
import { styled } from "@mui/material/styles";
import Cookies from 'js-cookie';
import '../translations/i18n';

const PREFIX = 'NewProject';

const classes = {
    modal: `${PREFIX}-modal`,
    container: `${PREFIX}-container`,
    projectImageSection: `${PREFIX}-projectImageSection`,
    projImage: `${PREFIX}-projImage`,
    technologyContainer: `${PREFIX}-technologyContainer`
};

const StyledModal = styled(Modal)((
    {
        theme
    }
) => ({
    [`&.${classes.modal}`]: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },

    [`& .${classes.container}`]: {
        backgroundColor: theme.palette.background.paper,
        borderRadius: '8px',
        paddingTop: '16px',
        paddingBottom: '16px',
        [theme.breakpoints.up('sm')]: {
            paddingTop: '24px',
            paddingBottom: '24px',
        },
    },

    [`& .${classes.projectImageSection}`]: {
        display: 'flex',
        gap: '16px',
    },

    [`& .${classes.projImage}`]: {
        objectFit: 'contain',
    },

    [`& .${classes.technologyContainer}`]: {
        display: 'flex',
        justifyContent: ''
    }
}));

const Input = styled('input')({
    display: 'none',
});

const initialState = {
    projTitleSk: '',
    projTitleEn: '',
    projUrl: '',
    projImage: null,
    projImageFile: null,
    projParagraphs: [{text_sk: '', text_en: ''}],
    projTechnologies: [{technology: '', link: ''}],
};

function newProjReducer(state, action) {
    switch (action.type) {
        case 'setProjTitleSk':
            return {
                ...state,
                projTitleSk: action.payload
            };
        case 'setProjTitleEn':
            return {
                ...state,
                projTitleEn: action.payload
            };
        case 'setProjUrl':
            return {
                ...state,
                projUrl: action.payload
            };
        case 'setProjImage':
            return {
                ...state,
                projImage: action.payload
            };
        case 'setNullProjImage':
            return {
                ...state,
                projImage: null
            };
        case 'setProjImageFile':
            return {
                ...state,
                projImageFile: action.payload
            };
        case 'setNullProjImageFile':
            return {
                ...state,
                projImageFile: null
            };
        case 'setProjParagraphs':
            return {
                ...state,
                projParagraphs: action.payload
            }
        case 'setProjTechnologies':
            return {
                ...state,
                projTechnologies: action.payload
            };
        default:
            throw new Error(`Passed type ${action.type} is not valid type for newProjReducer.`);
    }
}

function NewProject({opened, onClose, setNewProjectCallback}) {
    const EN_LANGUAGE = 'en';
    const SK_LANGUAGE = 'sk';

    const { t } = useTranslation();

    const [state, dispatch] = useReducer(newProjReducer, initialState);

    const handleFormSubmit = () => {
        console.log(state.projTitleSk);
        console.log(state.projTitleEn);
        console.log(state.projUrl);
        console.log(state.projImage);
        console.log(state.projParagraphs);
        console.log(state.projTechnologies);

        let projectData = new FormData();
        projectData.append('title_sk', state.projTitleSk);
        projectData.append('title_en', state.projTitleEn);
        projectData.append('link', state.projUrl);
        projectData.append('imageLink', state.projImageFile);
        projectData.append('projectparagraph_set', JSON.stringify(state.projParagraphs));
        projectData.append('projecttechnology_set', JSON.stringify(state.projTechnologies));
        
        const requestOptions = {
            method: 'POST',
            body: projectData,
            credentials: 'include',
            headers: {
                'X-Csrftoken': Cookies.get('csrftoken'),
            },
        }

        fetch('/api/projects', requestOptions)
        .then(response => {
            console.log(response);
            return response.json();
        })
        .then(resJson => {
            console.log(resJson);
            setNewProjectCallback(resJson);
        });
    }

    const handleProjTitleChange = (e, language) => {
        const { value } = e.target;
        console.log(value);
        if (language === SK_LANGUAGE) {
            dispatch({type: 'setProjTitleSk', payload: value});
        } else if (language === EN_LANGUAGE) {
            dispatch({type: 'setProjTitleEn', payload: value});
        }
    }

    const handleProjUrlChange = (e) => {
        const { value } = e.target;
        dispatch({type: 'setProjUrl', payload: value});
    }
    
    const onImageChange = e => {
        if (e.target.files && e.target.files[0]) {
            let reader = new FileReader();
            console.log('Before reader.onload');
            reader.onload = e => {
                console.log('onload:' + e.target.result);
                dispatch({type: 'setProjImage', payload: e.target.result});
            }
            reader.readAsDataURL(e.target.files[0]); 
            dispatch({type: 'setProjImageFile', payload: e.target.files[0]});
        }
    }

    const handleImageDelete = (e) => {
        e.preventDefault();
        dispatch({type: 'setNullProjImage'});
        dispatch({type: 'setNullProjImageFile'});
    }

    const renderImageAndDeleteButton = () => {
        return (
            <React.Fragment>
                <img src={state.projImage} alt="" width="50" height="50" className={classes.projImage} />
                <IconButton onClick={handleImageDelete} size="large"><HighlightOffIcon /></IconButton>
            </React.Fragment>
        );
    }

    const renderProjectParagraphs = () => {
        return state.projParagraphs.map((paragraphItem, i) => {
            return (
                <Grid item xs={12} key={i}>
                    <Typography>
                        {t('projects.paragraph')}
                    </Typography>
                    <Grid item xs={12}>
                        <TextField
                            name="text_sk"
                            label={t('projects.languageSk')}
                            variant="outlined"
                            multiline
                            fullWidth
                            style={{display: 'block'}}
                            value={paragraphItem.text_sk}
                            onChange={event => handleParagraphChange(event, i)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            name="text_en"
                            label={t('projects.languageEn')}
                            variant="outlined"
                            multiline
                            fullWidth
                            style={{display: 'block'}}
                            value={paragraphItem.text_en}
                            onChange={event => handleParagraphChange(event, i)}
                        />
                    </Grid>
                </Grid>
            );
        });
    }

    const handleParagraphChange = (event, i) => {
        const { value, name } = event.target;
        console.log(`Paragraph changed: value is ${value}, i is ${i}`);
        const currParagraphs = [...state.projParagraphs];
        console.log(`Current paragraphs: ${JSON.stringify(currParagraphs)}`);
        currParagraphs[i][name] = value;
        dispatch({type: 'setProjParagraphs', payload: currParagraphs});
    }

    const handleAddParagraph = (e) => {
        const currParagraphs = [...state.projParagraphs];
        currParagraphs.push({text_sk: '', text_en: ''});
        dispatch({type: 'setProjParagraphs', payload: currParagraphs});
    }

    const renderProjectTechnologies = () => {
        return state.projTechnologies.map((technologyItem, i) => {
            return(
                <React.Fragment key={i}>
                    <Grid item xs={12} sm={3}>
                        <TextField 
                            name="technology"
                            label={t('projects.projectTechnology')}
                            variant="outlined"
                            fullWidth
                            value={technologyItem.technology}
                            onChange={event => handleTechnologyChange(event, i)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={9} key={i}>
                        <TextField 
                            name="link"
                            label={t('projects.projectTechnologyUrl')}
                            variant="outlined"
                            type="url"
                            fullWidth
                            value={technologyItem.link}
                            onChange={event => handleTechnologyChange(event, i)}
                        />
                    </Grid>
                </React.Fragment>
            );
        });
    }

    const handleTechnologyChange = (event, i) => {
        const { value, name } = event.target;
        const currTechnologies = [...state.projTechnologies];
        currTechnologies[i][name] = value;
        dispatch({type: 'setProjTechnologies', payload: currTechnologies});
    }

    const handleAddTechnology = (e) => {
        const currTechnologies = [...state.projTechnologies];
        currTechnologies.push({technology: '', link: ''});
        dispatch({type: 'setProjTechnologies', payload: currTechnologies});
    }

    return (
        <StyledModal
                open={opened}
                onClose={onClose}
                className={classes.modal}
            >
            <Container className={classes.container} maxWidth='md'>
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <TextField
                            label={t('projects.projectNameSk')}
                            variant="outlined"
                            fullWidth
                            value={state.projTitleSk}
                            onChange={event => handleProjTitleChange(event, SK_LANGUAGE)}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            label={t('projects.projectNameEn')}
                            variant="outlined"
                            fullWidth
                            value={state.projTitleEn}
                            onChange={event => handleProjTitleChange(event, EN_LANGUAGE)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor="project-image" className={classes.projectImageSection}>
                            <Input
                                id="project-image"
                                accept="image/*"
                                type="file"
                                onChange={onImageChange}
                            />
                            <Button 
                                variant="contained" 
                                component="span">
                                    {t('projects.projectImage')}
                                </Button>
                            {state.projImage && renderImageAndDeleteButton()}
                        </label>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField 
                            label={t('projects.projectUrl')}
                            variant="outlined"
                            type="url"
                            fullWidth
                            onChange={event => handleProjUrlChange(event)}
                            value={state.projUrl}
                        />
                    </Grid>
                    {renderProjectParagraphs()}
                    <Grid item xs={12}>
                        <IconButton onClick={handleAddParagraph} size="large"><PlaylistAddIcon /></IconButton>
                    </Grid>
                    {renderProjectTechnologies()}
                    <Grid item xs={12}>
                        <IconButton onClick={handleAddTechnology} size="large"><PlaylistAddIcon /></IconButton>
                    </Grid>
                    <Grid item xs={12}>
                        <Button 
                            variant="contained"
                            onClick={handleFormSubmit}
                        >
                            {t('projects.submit')}
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </StyledModal>
    );
}

export default NewProject;