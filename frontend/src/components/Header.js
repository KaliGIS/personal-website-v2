import { AppBar, Toolbar, IconButton, Button, Menu, MenuItem } from '@mui/material';
import { styled } from '@mui/material/styles';
import { SK, GB } from 'country-flag-icons/react/3x2';
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import MoreIcon from '@mui/icons-material/MoreVert';
import i18n from '../translations/i18n';
import { useTranslation } from 'react-i18next';
import { AppContext } from '../App';

const PREFIX = 'Header';

const classes = {
    toolbar: `${PREFIX}-toolbar`,
    sectionDesktop: `${PREFIX}-sectionDesktop`,
    sectionMobile: `${PREFIX}-sectionMobile`,
};

const Root = styled('div')((
    {
        theme
    }
) => ({
    [`& .${classes.toolbar}`]: {
        justifyContent: 'space-between',
    },

    [`& .${classes.sectionDesktop}`]: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
        },
    },

    [`& .${classes.sectionMobile}`]: {
        display: 'flex',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
}));

const StyledButton = styled(Button)({
    color: 'white'
});

const StyledMoreIcon = styled(MoreIcon)({
    color: 'white'
});

function Header() {
    const { t } = useTranslation();
    const {loggedUser, setCurrentLanguage, logoutCallback } = useContext(AppContext);
    console.log('Logged user in header: ' + loggedUser);

    const [anchorElement, setAnchorElement] = useState(null);

    const handleMobileMenuOpen = (event) => {
        setAnchorElement(event.currentTarget);
    };

    const handleMobileMenuClose = () => {
        setAnchorElement(null);
    };

    const handleChangeLanguage = (event) => {
        event.preventDefault();
        let lang = event.currentTarget.value;
        i18n.changeLanguage(lang);
        setCurrentLanguage(lang);
    };

    const renderLogin = () => {
        return(
            <StyledButton to='/login' component={Link}>
                { t('header.loginNav') }
            </StyledButton>
        );
    }

    const renderLogout = () => {
        return(
            <StyledButton
                onClick={logoutCallback}
            >
                { t('header.logoutNav') }
            </StyledButton>
        );
    }

    return (
        <Root>
            <AppBar position='static'>
                <Toolbar className={classes.toolbar}>
                    <div>
                        <StyledButton to='/projects' component={Link}>
                            { t('header.projectsNav') }
                        </StyledButton>
                        {
                            loggedUser ? renderLogout() : renderLogin()
                        }
                    </div>
                    <div className={classes.sectionDesktop}>
                        <IconButton value='sk' onClick={handleChangeLanguage} size="large">
                            <SK title='Slovak' className='countryFlag'/>
                        </IconButton>
                        <IconButton value='en' onClick={handleChangeLanguage} size="large">
                            <GB title='English' className='countryFlag'/>
                        </IconButton>
                    </div>
                    <div className={classes.sectionMobile}>
                        <IconButton onClick={handleMobileMenuOpen} size="large">
                            <StyledMoreIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            <Menu
                id="simple-menu"
                anchorEl={anchorElement}
                keepMounted
                open={Boolean(anchorElement)}
                onClose={handleMobileMenuClose}
            >
                <MenuItem>
                    <IconButton value='sk' onClick={handleChangeLanguage} size="large">
                        <SK title='Slovak' className='countryFlag'/>
                    </IconButton>
                </MenuItem>
                <MenuItem>
                    <IconButton value='en' onClick={handleChangeLanguage} size="large">
                        <GB title='English' className='countryFlag'/>
                    </IconButton>
                </MenuItem>
            </Menu>
        </Root>
    );
}

export default Header;
