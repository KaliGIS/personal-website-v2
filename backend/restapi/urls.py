from django.urls import path
from .views import CvView, ProjectListView, ProjectDetailView, LoginView

urlpatterns = [
    path('cv', CvView.as_view()),
    path('projects', ProjectListView.as_view()),
    path('projects/<int:id>', ProjectDetailView.as_view()),
    path('login', LoginView.as_view()),
]