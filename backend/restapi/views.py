from django.conf import settings
from django.shortcuts import render
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from django.contrib.auth import authenticate
from django.conf import settings
from django.middleware import csrf
from rest_framework.parsers import MultiPartParser, FileUploadParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from .serializers import CvSerializer, ProjectSkSerializer, ProjectEnSerializer, CreateProjectSerializer
from .models import Cv
from .models import Project
from rest_framework_simplejwt.tokens import RefreshToken
from PIL import Image
import json, datetime

def getTokensForUser(user):
    tokens = RefreshToken.for_user(user)
    return {
        'refresh': str(tokens),
        'access': str(tokens.access_token)
    }

# Create your views here.
class CvView(generics.ListAPIView):
    queryset = Cv.objects.all()
    serializer_class = CvSerializer

class ProjectListView(APIView):
    parser_classes = [MultiPartParser, FileUploadParser]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request, format = None):
        language = request.META.get('HTTP_ACCEPT_LANGUAGE')
        queryset = Project.objects.all()
        if language == 'en':
            serializer_class = ProjectEnSerializer(queryset, many = True)
        else:
            serializer_class = ProjectSkSerializer(queryset, many = True)
        return Response(serializer_class.data)

    def post(self, request, format = None):
        print('INCOMING DATA:', request.data, flush = True)
        parsedData = self.parseRequestNestedJsonData(request.data)
        print('POST REQUEST PARSED DATA:', parsedData, flush = True)
        newProject = CreateProjectSerializer(data = parsedData)
        if newProject.is_valid():
            newProject.save()
            return Response(newProject.data, status = status.HTTP_201_CREATED)
        else:
            print('ERROR MESSAGES:', newProject.error_messages, flush = True)
            print('ERRORS:', newProject.errors, flush = True)
            return Response(newProject.errors, status=status.HTTP_400_BAD_REQUEST)
        
    def parseRequestNestedJsonData(self, requestData):
        parsedData = {}
        if 'projectparagraph_set' in requestData:
            print('projectparagraph_set[0]:', requestData['projectparagraph_set'], flush = True)
            projectParagraphData = json.loads(requestData['projectparagraph_set'])
            parsedData['projectparagraph_set'] = projectParagraphData
        if 'projecttechnology_set' in requestData:
            projectTechnologyData = json.loads(requestData['projecttechnology_set'])
            parsedData['projecttechnology_set'] = projectTechnologyData
        parsedData['title_sk'] = requestData['title_sk']
        parsedData['title_en'] = requestData['title_en']
        parsedData['link'] = requestData['link']
        if requestData['imageLink'] != 'null':
            parsedData['imageLink'] = requestData['imageLink']
        return parsedData

class ProjectDetailView(APIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    def getProjectById(self, id):
        try:
            return Project.objects.get(id = id)
        except Project.DoesNotExist:
            raise Http404

    def get(self, request, id, format = None):
        project = self.getProjectById(id)
        language = request.META.get('HTTP_ACCEPT_LANGUAGE')
        if language == 'en':
            serializer = ProjectEnSerializer(project)
        else:
            serializer = ProjectSkSerializer(project)
        return Response(serializer.data)

    def delete(self, request, id, format = None):
        project = self.getProjectById(id)
        project.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class LoginView(APIView):
    def post(self, request, format = None):
        print('In LoginView post method', flush=True)
        data = request.data
        userName = data.get('username', None)
        password = data.get('password', None)
        user = authenticate(username = userName, password = password)
        if user is not None:
            if user.is_active:
                response = Response()
                tokenData = getTokensForUser(user)
                expires = datetime.datetime.utcnow() + settings.SIMPLE_JWT['ACCESS_TOKEN_LIFETIME']
                print('Setting expires to cookie:', expires, flush=True)
                response.set_cookie(
                    key = settings.SIMPLE_JWT['AUTH_COOKIE'],
                    value = tokenData['access'],
                    expires = expires,
                    secure = settings.SIMPLE_JWT['AUTH_COOKIE_SECURE'],
                    httponly = settings.SIMPLE_JWT['AUTH_COOKIE_HTTP_ONLY'],
                    samesite = settings.SIMPLE_JWT['AUTH_COOKIE_SAMESITE']
                )
                csrf.get_token(request)
                response.data = {'message': 'Successful login', 'user': user.get_username()}
                return response
            else:
                return Response({'message': 'This account is not active.'}, status = status.HTTP_404_NOT_FOUND)
        return Response({'message': 'Invalid username or password'}, status = status.HTTP_404_NOT_FOUND)

    def delete(self, request, format = None):
        print('In LoginView delete (logout) method', flush = True)
        response = Response()
        response.set_cookie(
            key = settings.SIMPLE_JWT['AUTH_COOKIE'],
            expires = datetime.datetime.utcnow() + datetime.timedelta(seconds = 5),
            secure = settings.SIMPLE_JWT['AUTH_COOKIE_SECURE'],
            httponly = settings.SIMPLE_JWT['AUTH_COOKIE_HTTP_ONLY'],
            samesite = settings.SIMPLE_JWT['AUTH_COOKIE_SAMESITE']
        )
        csrf.get_token(request)
        response.data = {'message': 'Successful logout'}
        response.status_code = status.HTTP_204_NO_CONTENT
        return response