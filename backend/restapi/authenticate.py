from rest_framework_simplejwt.authentication import JWTAuthentication
from django.conf import settings
from rest_framework.authentication import CSRFCheck
from rest_framework import exceptions

def enforceCsrf(request):
    check = CSRFCheck()
    check.process_request(request)
    reason = check.process_view(request, None, (), {})
    if reason:
        raise exceptions.PermissionDenied(f'CSRF Failed: {reason}')

class CustomAuthentication(JWTAuthentication):
    def authenticate(self, request):
        print('In Authenticate')
        header = self.get_header(request)

        if header is None:
            rawToken = request.COOKIES.get(settings.SIMPLE_JWT['AUTH_COOKIE']) or None
        else:
            rawToken = self.get_raw_token(header)
        
        if rawToken is None:
            return None
        
        validatedToken = self.get_validated_token(rawToken)
        enforceCsrf(request)
        return (self.get_user(validatedToken), validatedToken)