# Generated by Django 3.2.8 on 2021-11-04 19:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restapi', '0009_auto_20211102_1804'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='imageLink',
            field=models.ImageField(default='/code/media/noProjectImage.png', max_length=512, upload_to=''),
        ),
    ]
