from django.db import models
from django.conf import settings

# Create your models here.
class Cv(models.Model):
    section_id = models.IntegerField()
    section_cat = models.IntegerField(default=1)
    parent_id = models.IntegerField()
    section_name = models.CharField(max_length=256)
    section_text = models.CharField(max_length=1024)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

class Project(models.Model):
    title_sk = models.CharField(max_length=128)
    title_en = models.CharField(max_length=128)
    link = models.CharField(max_length=128)
    imageLink = models.ImageField(max_length=512, default=settings.DEFAULT_PROJECT_IMAGE)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
class ProjectParagraph(models.Model):
    text_sk = models.CharField(max_length=2048)
    text_en = models.CharField(max_length=2048)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

class ProjectTechnology(models.Model):
    technology = models.CharField(max_length=2048)
    link = models.CharField(max_length=128)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)