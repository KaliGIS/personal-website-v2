from rest_framework import serializers
from .models import Cv, Project, ProjectParagraph, ProjectTechnology

class CvSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cv
        fields = ('id', 'section_id', 'section_cat', 'parent_id', 'section_name', 'section_text', 'created', 'modified')

class ProjectParagraphSkSerializer(serializers.ModelSerializer):
    text = serializers.CharField(source = 'text_sk')

    class Meta:
        model = ProjectParagraph
        fields = ['text', 'created', 'modified']

class ProjectParagraphEnSerializer(serializers.ModelSerializer):
    text = serializers.CharField(source = 'text_en')

    class Meta:
        model = ProjectParagraph
        fields = ['text', 'created', 'modified']

class ProjectTechnologySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectTechnology
        fields = ['technology', 'link', 'created', 'modified']

class ProjectSkSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source = 'title_sk')
    projectparagraph_set = ProjectParagraphSkSerializer(many = True, read_only = True)
    projecttechnology_set = ProjectTechnologySerializer(many = True, read_only = True)

    class Meta:
        model = Project
        fields = ['id','title', 'link', 'imageLink', 'projectparagraph_set', 'projecttechnology_set', 'created', 'modified']

class ProjectEnSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source = 'title_en')
    projectparagraph_set = ProjectParagraphEnSerializer(many = True, read_only = True)
    projecttechnology_set = ProjectTechnologySerializer(many = True, read_only = True)

    class Meta:
        model = Project
        fields = ['id', 'title', 'link', 'imageLink', 'projectparagraph_set', 'projecttechnology_set', 'created', 'modified']

class ProjectParagraphSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectParagraph
        fields = ['text_sk', 'text_en', 'created', 'modified']

class CreateProjectSerializer(serializers.ModelSerializer):
    projectparagraph_set = ProjectParagraphSerializer(many = True)
    projecttechnology_set = ProjectTechnologySerializer(many = True)

    class Meta:
        model = Project
        fields = ['id', 'title_sk', 'title_en', 'link', 'imageLink', 'projectparagraph_set', 'projecttechnology_set']

    def create(self, validated_data):
        print('CREATE CALLED', flush = True)
        print(validated_data, flush = True)
        projectParagraphData = validated_data.pop('projectparagraph_set')
        projecttechnologyData = validated_data.pop('projecttechnology_set')
        project = Project.objects.create(**validated_data)
        for paragraph in projectParagraphData:
            ProjectParagraph.objects.create(project = project, **paragraph)
        for technology in projecttechnologyData:
            ProjectTechnology.objects.create(project = project, **technology)
        print(project)
        return project